package com.cgi.ales.aggregationservice.controllers;

import com.cgi.ales.aggregationservice.servicenow.GetTicketByIdFromServiceNow;
import com.cgi.ales.aggregationservice.servicenow.TicketDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/aggr")
public class ServiceNowController {

    @Autowired
    GetTicketByIdFromServiceNow service;

    @GetMapping(path = "/{id}")
    public ResponseEntity<TicketDto> getTicket(@PathVariable("id") Long id) throws IOException {
        return new ResponseEntity<>(service.getTicketById(id), HttpStatus.OK);
    }
}