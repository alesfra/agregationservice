package com.cgi.ales.aggregationservice.servicenow;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GetTicketByIdFromServiceNow {

    final String REST_URI = "http://localhost:8080/ales-servicenow/api/v1/tickets/";
    private RestTemplate restTemplate = new RestTemplate();

    public TicketDto getTicketById(Long id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity(httpHeaders);

        ResponseEntity<TicketDto> ticketDto = restTemplate.exchange
                (REST_URI + id,
                        HttpMethod.GET,
                        httpEntity,
                        TicketDto.class);
        return ticketDto.getBody();
    }
}